const {SecretsManagerClient, GetSecretValueCommand, UpdateSecretCommand} = require('@aws-sdk/client-secrets-manager');
const fs = require("fs");

const SecretManager = {
    getCredential: async (secretId, region) => {
        var response = await SecretManager.getSecret(secretId, region);
        // console.log({response: response, sid: secretId, region: region});
        return JSON.parse(response.SecretString);
    },

    updateCredential: async (key, secret, secretId, region) => {
        const command = new UpdateSecretCommand({
            SecretId: secretId,
            SecretString: JSON.stringify({key: key, secret: secret})
        })

        const client = new SecretsManagerClient({
            region: region,
        })

        try {
            var response = await client.send(command);
            return response;
        } catch (error) {
            console.error(error);
        }
    },

    getSecret: async (secretId, region) => {
        var command = new GetSecretValueCommand({
            SecretId: secretId,
        });

        const client = new SecretsManagerClient({
            region: region
        });

        try {
            var result = await client.send(command);
            return result;
        } catch (error) {
            console.log(error);
        }
    },

    writeSecretToFile: async (key, secret) => {
        // file system module to perform file operations
        const content = {
            Version: 1,
            AccessKeyId: key,
            SecretAccessKey: secret,
        }
        // stringify JSON Object
        var jsonContent = JSON.stringify(content);
        var pathFileName = SecretManager.credentialFilePath;
        try {
            fs.writeFileSync(pathFileName, jsonContent);
        } catch (error) {
            console.error(error);
        }
    },

    readSecretFromFile: async () => {
        var pathFileName = SecretManager.credentialFilePath;
        try {
            var content = fs.readFileSync(pathFileName, 'utf-8');
            return content;
        } catch (error) {
            console.error(error);
        }
    },

    credentialFilePath: __dirname + `./../repo/credentials`,
}


module.exports = SecretManager;