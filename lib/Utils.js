const SecretManager = require("../lib/SecretManager");
const Utils = {
    refreshCredential: async (secretId, region) => {
        var result = await SecretManager.getCredential(secretId, region);
        await SecretManager.writeSecretToFile(result.key, result.secret);
    }
}
module.exports = Utils;