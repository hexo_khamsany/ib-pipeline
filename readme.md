# User Manual

## Requirement
1. NodeJS, please install nodejs from https://nodejs.org/en/

2. ~~Log on to LeCroy AWS console and get the AWS key & secret and follow the instruction how to use the key & secret
   in your local machine.~~

2. key & secret for upload & sync rotates at specific period, shared file credentials will never work in this update, 
it replaced with process control credential. Please refer https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-sourcing-external.html for details
In this code the configuration used, you may leave the `jenkins` to avoid any code changes
```
[profile jenkins]
credential_process = node ROOT_PATH_OF_CODE/app credentials
```
3. For rotate AWS IAM access key, please create a user with proper permission to create & remove the access key and read & write permission to Secret Manager, for this purpose the aws key & secret SHOULD NOT rotate
for rotate please configure the Credential as shared file in `.aws` folder, please refer here https://docs.aws.amazon.com/ses/latest/DeveloperGuide/create-shared-credentials-file.html
4. For accessing AWS IAM access key from Secret Manager, it also required as number #3 mentioned but with permission able at least accessing the secret value

## Installation
1. checkout the repo to your computer
2. run `npm install` this will install all necessary

## Usages:
### 1. Uploading installer component files to S3
```shell
node app upload <path>

make Upload all installer component files to S3 bucket

Options:
  --version              Show version number                           [boolean]
  --help                 Show help                                     [boolean]
  --bucket               The S3 bucket name     [string] [default: "psginstallerdev"]
  --bucketRootDirectory  The S3 root directory to upload to
                                                      [string] [default: "repo"]
  --sourcePathMarker     The marker for folder structure base on local source.
                         e.g if source is
                         C:/documents/my_build/PETracer/1.000/FILES, and the
                         marker is PETracer, the file structure will be sync in
                         bucket is
                         BUCKET/BUCKET_ROOT_DIRECTORY/PETracer/1.000/FILES
                                                  [string] [default: "PETracer"]
  --region               The S3 bucket region    [string] [default: "us-west-1"]

example:
node app.js upload C:\\Users\\khams\\lecroy-automation\\files\\PETracer\\22.0.0015
```

### 2. Sync development S3 bucket of components files to production S3 bucket
```shell
app.js sync <revision>

To sync installer components file in development bucket to production bucket

Options:
  --version                  Show version number                       [boolean]
  --help                     Show help                                 [boolean]
  --sourcePathMarker         The marker for folder structure base on S3 source.
                             e.g if source in
                             S3://psginstallerdev/repo/PETracer/REVISION/FILES,
                             to sync to other Bucket is repo/PETracer
                                             [string] [default: "repo/PETracer"]
  --destinationBucket        The destination of S3 bucket to be sync
                                          [string] [default: "psginstallerprod"]
  --sourceBucket             The source of S3 bucket to be sync
                                           [string] [default: "psginstallerdev"]
  --sourceBucketRegion       The source S3 bucket region
                                                 [string] [default: "us-west-1"]
  --destinationBucketRegion  The destination S3 bucket region
                                                 [string] [default: "us-west-1"]
                                                 
example:
node app.js sync 22.0.0015

```

### 3. Rotate IAM key & secret
![secret rotation workflow](https://prod-cdn.albaloo.com/offers/rotates-secret.JPG)
```shell
To rotate specific user IAM access key & secret

Options:
--version               Show version number                          [boolean]
--help                  Show help                                    [boolean]
--username              The IAM username to rotate
[string] [default: "lecroy_installer"]
--viewCurrentList                                   [boolean] [default: false]
--region                AWS Secret Manager region
[string] [default: "us-east-1"]
--credentialSecretPath  AWS Secret Manager's secret id
[string] [default: "develop/lecroyib/aws"]

example:
node app rotate-access
```
### 4. Utilities (essential for monitoring or checking)
#### 4.1 check current credential for upload & sync 
```shell
node app credentials

output:
{"Version":1,"AccessKeyId":"AKIAQLXHPQLHRPSLMQR5","SecretAccessKey":"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"}
```
#### 4.2 check current active IAM key for the user
```shell
node app rotate-access --viewCurrentList=true

output:
[
  {
    UserName: 'lecroy_installer',
    AccessKeyId: 'AKIAQLXHPQLHRPSLMQR5',
    Status: 'Active',
    CreateDate: 2021-08-17T18:09:54.000Z
  }
]
```