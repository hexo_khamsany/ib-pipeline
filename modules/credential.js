const SecretManager = require("../lib/SecretManager");
const fs = require('fs');
const process = require('process')

exports.command = 'credentials'

exports.describe = 'Sourcing AWS credentials from external processes'

exports.handler = async (argv) => {
    var response = await SecretManager.readSecretFromFile();
    // console.log({response:response}); return;
    // process.stdout.write(response);
    // return;
    var credentials = JSON.parse(response);
    // requirement from aws
    // reference: https://docs.aws.amazon.com/cli/latest/topic/config-vars.html#sourcing-credentials-from-external-processes
    // var response = {
    //     "Version": 1,
    //     "AccessKeyId": credentials.AccessKeyId,
    //     "SecretAccessKey": credentials.SecretAccessKey,
    //     // "SessionToken": "",
    //     // "Expiration": "",
    // };
    process.stdout.write(JSON.stringify(credentials));
    // return 1010;

}