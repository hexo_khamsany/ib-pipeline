const {S3Client, CopyObjectCommand, ListObjectsCommand} = require("@aws-sdk/client-s3");
const Utils = require("../lib/Utils");

exports.command = 'sync <revision>'

exports.describe = 'To sync installer components file in development bucket to production bucket'

exports.builder = {
    sourcePathMarker: {
        default: 'repo/PETracer',
        description: 'The marker for folder structure base on S3 source. e.g if source in S3://psginstallerdev/repo/PETracer/REVISION/FILES, to sync to other Bucket is repo/PETracer',
        type: 'string',
    },
    destinationBucket: {
        default: 'psginstallerprod',
        description: 'The destination of S3 bucket to be sync',
        type: 'string'
    },
    sourceBucket: {
        default: 'psginstallerdev',
        description: 'The source of S3 bucket to be sync',
        type: 'string'
    },
    sourceBucketRegion: {
        default: 'us-west-1',
        description: 'The source S3 bucket region',
        type: 'string',
    },
    destinationBucketRegion: {
        default: 'us-west-1',
        description: 'The destination S3 bucket region',
        type: 'string',
    },
    credentialSecretPath: {
        default: 'develop/lecroyib/aws',
        description: 'The secret manager secret id',
        type: 'string',
    },
    credentialProfile : {
        default: 'jenkins',
        type: 'string',
    }
}


exports.handler = async function (argv) {
    // refresh credentials because the key & secret rotates
    await Utils.refreshCredential(argv.credentialSecretPath, argv.region)
    run(argv.revision, argv);
}

/**
 * global
 */
var arguments = {};

const copy = async (sourcePath) => {
    const bucket = arguments.destinationBucket;
    var destinationArray = sourcePath.split('/');
    destinationArray.splice(0, 1);
    var destination = destinationArray.join("/");
    console.log(`copying ${sourcePath} ----> ${bucket}/${destination}`);
    const command = new CopyObjectCommand({
        Bucket: bucket,
        Key: destination,
        CopySource: sourcePath
    });

    const client = new S3Client({
        region: arguments.destinationBucketRegion,
        profile: arguments.credentialProfile
    });

    try {
        return client.send(command);
    } catch (error) {
        console.error(error);
    }
}

/**
 * Main
 * @param bucket
 * @param revision
 * @param argv
 * @returns {Promise<void>}
 */
const run = async (revision, argv) => {
    arguments = argv;
    const bucket = argv.sourceBucket;
    const command = new ListObjectsCommand({
        Bucket: bucket,
        Marker: `${arguments.sourcePathMarker}/${revision}`,
        Prefix: `${arguments.sourcePathMarker}/${revision}`,
    })

    const client = new S3Client({
        region: arguments.sourceBucketRegion,
        profile: arguments.credentialProfile
    });

    try {
        const result = await client.send(command);
        var totalLength = result.Contents.length;
        for (var i = 0; i < totalLength; i++) {
            console.log(`[${i + 1} of ${totalLength}]`);
            var copied = await copy(`${bucket}/` + result.Contents[i].Key, revision);
        }
    } catch (error) {
        console.error({error: error});
    }
}

// run('dev-lecroy', '22.0.0015');