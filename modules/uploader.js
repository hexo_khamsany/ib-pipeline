const fs = require("fs")
const path = require("path")
const {S3Client, PutObjectCommand, ListObjectsCommand} = require("@aws-sdk/client-s3");
const SecretManager = require("../lib/SecretManager");
const Utils = require("../lib/Utils");

exports.command = 'upload <path>'
exports.describe = 'Upload all installer component files to S3 bucket'
exports.builder = {
    bucket: {
        default: 'psginstallerdev',
        description: 'The S3 bucket name',
        type: 'string',
    },
    bucketRootDirectory: {
        default: 'repo',
        description: 'The S3 root directory to upload to',
        type: 'string',
    },
    sourcePathMarker: {
        default: 'PETracer',
        description: 'The marker for folder structure base on local source. e.g if source is C:/documents/my_build/PETracer/1.000/FILES, and the marker is PETracer, the file structure will be sync in bucket is BUCKET/BUCKET_ROOT_DIRECTORY/PETracer/1.000/FILES',
        type: 'string',
    },
    sourcePathMarkerPathName: {
        default: 'PCIe Protocol Suite',
        type: 'string'
    },
    region: {
        default: 'us-west-1',
        description: 'The S3 bucket region',
        type: 'string',
    },
    credentialSecretPath: {
        default: 'develop/lecroyib/aws',
        description: 'The secret manager secret id',
        type: 'string',
    },
    credentialProfile: {
        default: 'jenkins',
        type: 'string',
    },
    fileDestinationPath: {
        type: 'string',
        description: 'The destination path if file'
    },

}

exports.handler = async function (argv) {
    // refresh the credentials, because we rotate the credential we need to always refresh to get updated credential
    // await Utils.refreshCredential(argv.credentialSecretPath, argv.region);
    const path = argv.path;
    if (fs.lstatSync(path).isDirectory()) {
        uploadFiles(path, argv);
    } else {
        uploadFile(path, argv);
    }

}
/**
 * global variable
 */
var arguments = {};
var credential = {};

const getAllFiles = (dirPath, arrayOfFiles) => {
    files = fs.readdirSync(dirPath);
    arrayOfFiles = arrayOfFiles || []

    files.forEach(function (file) {
        if (fs.statSync(dirPath + "/" + file).isDirectory()) {
            arrayOfFiles = getAllFiles(dirPath + "/" + file, arrayOfFiles)
        } else {
            // comment out useful for relative path.
            // arrayOfFiles.push(path.join(__dirname, dirPath, "/", file))
            arrayOfFiles.push(path.join(dirPath, "/", file));
        }
    })

    return arrayOfFiles
}
const uploadFile = async (path, argv) => {
    arguments = argv;
    await uploadToS3(path, argv.fileDestinationPath);
}
const uploadFiles = async (dirPath, argv) => {
    arguments = argv;
    var filePaths = getAllFiles(dirPath);
    var destinationPath = '';
    for (var i = 0; i < filePaths.length; i++) {
        destinationPath = getDestinationPath(filePaths[i]);
        console.log(`[${i + 1} of ${filePaths.length}]`);
        try {
            const result = await uploadToS3(filePaths[i], destinationPath);
            console.log("successfully upload")
        } catch (error) {
            console.error(error);
        }
    }
}

const uploadToS3 = async (source, destination) => {
    bucket = arguments.bucket;
    const bucketPath = arguments.bucketRootDirectory;
    const fileStream = fs.createReadStream(source);
    const command = new PutObjectCommand({
        Bucket: bucket,
        // Add the required 'Key' parameter using the 'path' module.
        Key: `${bucketPath}/${destination}`,
        // Add the required 'Body' parameter
        Body: fileStream,
    });

    const client = new S3Client({
        region: arguments.region,
        profile: arguments.credentialProfile,
    });

    try {
        console.log(`Uploading ${source} ---> ${bucket}/${bucketPath}/${destination}`);
        return client.send(command);

    } catch (error) {
        console.error({error: error});
    }
}

const getDestinationPath = (localFilePath) => {
    // only works on windows system
    var pathArray = localFilePath.split("\\");
    pathArray.every((value, key) => {
        if (value == arguments.sourcePathMarker) {
            pathArray.splice(0, key + 1, arguments.sourcePathMarkerPathName);
            return false;
        }
        return true;
    });
    return pathArray.join('/');
}
