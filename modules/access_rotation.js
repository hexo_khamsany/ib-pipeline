const {
    IAMClient,
    ListAccessKeysCommand,
    CreateAccessKeyCommand,
    UpdateAccessKeyCommand,
    DeleteAccessKeyCommand
} = require('@aws-sdk/client-iam');

const SecretManager = require("../lib/SecretManager");


exports.command = 'rotate-access'

exports.describe = 'To rotate specific user IAM access key & secret'

exports.builder = {
    username: {
        default: 'lecroy_installer',
        description: 'The IAM username to rotate',
        type: 'string',
    },
    viewCurrentList: {
        default: false,
        type: 'boolean'
    },
    region: {
        default: 'us-east-1',
        description: 'AWS Secret Manager region',
        type: 'string'
    },
    credentialSecretPath: {
        default: 'develop/lecroyib/aws',
        description: 'AWS Secret Manager\'s secret id',
        type: 'string'
    }
}

exports.handler = async function (argv) {
    if (argv.viewCurrentList == true) {
        view(argv);
    } else {
        run(argv);
    }

}

var arguments = {};
var username = '';
var currentAccess = {};

const run = async (argv) => {
    arguments = argv;
    username = argv.username;
    try {
        // 1. get all users access key limited max 2
        var list = await getList();
        // console.log({result: list.AccessKeyMetadata});
        // 2. deactivate any current access active key
        deactivateAllCurrentKeys(list.AccessKeyMetadata);
        // 3. create the new access key
        var response = await createNewAccessIdAndSecret();
        // 4. update the Secret Manager value (rotating)
        console.log(`Updating new access key to Secret Manager: ${argv.credentialSecretPath}`);
        var rotateResponse = await SecretManager.updateCredential(response.AccessKey.AccessKeyId, response.AccessKey.SecretAccessKey, argv.credentialSecretPath, argv.region)
        // console.log({rotate: rotateResponse});
        // 5. remove all the inactive key
        removeInActiveAccessKeys();

    } catch (error) {
        console.error({getListError: error});
    }
}

const getList = async () => {
    const command = new ListAccessKeysCommand({
        UserName: username
    });

    const client = new IAMClient({
        Region: arguments.region,
    });

    return client.send(command);
}

/**
 *
 * @param keys [{UserName:USERNAME, AccessKeyId:ACCESS_KEY_ID, Status:Active|Inactive, CreateDate:CREATE_DATE}]
 * @returns {Promise<void>}
 */
const deactivateAllCurrentKeys = async (keys) => {
    var input = {
        UserName: username,
        Status: "Inactive",
        AccessKeyId: ''
    };

    const client = new IAMClient({
        Region: arguments.region
    });

    for (var i = 0; i < keys.length; i++) {
        var value = keys[i];
        if (value.Status == 'Active') {
            input.AccessKeyId = value.AccessKeyId;
            const command = new UpdateAccessKeyCommand(input);
            console.log(`Deactivate Access Key Id: ${value.AccessKeyId}..`)
            try {
                await client.send(command);
            } catch (error) {
                console.error({deactivateError: error})
            }

        }
    }
}

const createNewAccessIdAndSecret = async () => {
    const command = new CreateAccessKeyCommand({
        UserName: username,
    });

    const client = new IAMClient({
        Region: arguments.region,
    });

    return client.send(command);
}

const removeInActiveAccessKeys = async () => {
    var response = await getList();
    var keys = response.AccessKeyMetadata;
    var input = {
        UserName: username,
        AccessKeyId: '',
    }
    for (var i = 0; i < keys.length; i++) {
        var value = keys[i];
        if (value.Status == 'Inactive') {
            input.AccessKeyId = value.AccessKeyId;
            var command = new DeleteAccessKeyCommand(input);
            console.log(`Removing inactive Access Key Id: ${value.AccessKeyId}`)
            var client = new IAMClient({
                Region: arguments.region
            });
            await client.send(command);
        }
    }
}

const view = async (argv) => {
    arguments = argv;
    username = argv.username;
    try {
        var result = await getList();
        console.log(result.AccessKeyMetadata);
    } catch (error) {
        console.error({view: error});
    }
}