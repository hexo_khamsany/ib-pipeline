const fs = require("fs")
const xml2js = require("xml2js")
const process = require('process')

exports.command = 'identify-version'
exports.describe = 'Identify installer version base on xml file'
exports.builder = {
    'filePathName': {
        default: 'C:\\Users\\khams\\lecroy\\PETracer\\config\\config.xml',
        type: 'string',
        description: 'The file path of the config.xml file of the installer'
    },
    'setEnvironment': {
        default: false,
        type: 'boolean',
    },
    'environmentKey': {
        default: 'INSTALLER_VERSION',
        type: 'string'
    }
}

exports.handler = async (argv) => {
    const parser = new xml2js.Parser({attrkey: "Installer"});
    let xml_string = fs.readFileSync(argv.filePathName, "utf8");
    parser.parseString(xml_string, function (error, result) {
        if (error === null) {
            if (('Installer' in result) && ('Version' in result.Installer)) {
                process.stdout.write(result.Installer.Version[0].toString());
            }
        } else {
            console.log(error);
        }
    });
}