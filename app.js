const yargs = require('yargs');

yargs
    .command(require('./modules/uploader'))
    .command(require('./modules/synchronizer'))
    .command(require('./modules/access_rotation'))
    .command(require('./modules/credential'))
    .command(require('./modules/version_identifier'))
    .help()
    .argv;
